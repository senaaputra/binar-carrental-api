# Test-Driven Development

* BCR API: https://binar-car-rental-app.herokuapp.com
* BCR API documentation: https://binar-car-rental-app.herokuapp.com/documentation/

## Tech Stack
  * Node.js 16.15.0 (Runtime Environment)
  * Express 4.18.1 (HTTP Server)
  * Nodemon 2.0.16 (Server Runner)
  * Jest 28.1.0 (Testing Library)

## Prerequisite

These are things that you need to install on your machine before proceed to installation
* [Node.js](https://nodejs.org/)
* [NPM (Package Manager)](https://www.npmjs.com/)
* [PostgreSQL](https://www.postgresql.org/) 
* [Postman](https://www.postman.com/)


## Installation

Clone this repository

```bash
cd desired_directory/
git clone https://gitlab.com/senaaputra/back-end
cd back-end
```

Download all the package and it's dependencies
```bash
npm install 
```

Config the Database
```bash
cd app/
cat config/config.json
```

Create The Database
```bash
npm run db:create
```

Run The Migration
```bash
npm run db:migrate
``` 

Seed The Data
```bash
npm run db:seed
``` 


## Run The Server

Run the following command to start the server

```bash
  npm run start
```

Open [http://localhost:8000](http://localhost:8000) to view it in your browser.

## Test The Server

Run the following command to start test the server using Jest 
```bash
  npm test
```


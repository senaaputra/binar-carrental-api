const AuthenticationController = require("../../app/controllers/AuthenticationController");
const {
    EmailNotRegisteredError, 
    InsufficientAccessError, 
    RecordNotFoundError, 
    WrongPasswordError,
    EmailAlreadyTakenError
} = require("../../app/errors");
const { User, Role } = require("../../app/models");
const jwt = require("jsonwebtoken");
const bcrypt = require("bcryptjs");

describe("AuthenticationController", () => {
    describe("#authorize", () => {
      test("should run next if the token is correct", () => {
        const mockRole = new Role({
          id: 1,
          name: "ADMIN",
        });
        const mockUser = new User({
          id: 4,
          name: "example",
          email: "example@email.com",
          image: null,
          encryptedPassword: "password",
          roleId: 2,
        });
  
        const mockReq = {
          headers: {
            authorization:
              "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6NywibmFtZSI6ImZyaXphIiwiZW1haWwiOiJleGFtcGxlQGVtYWlsLmNvbSIsImltYWdlIjpudWxsLCJyb2xlIjp7ImlkIjoxLCJuYW1lIjoiQ1VTVE9NRVIifSwiaWF0IjoxNjU0OTM5MjY5fQ.J3zzqC14IbX-Ih0oVt8ThL00EVS5nTfgCP7CzFK0-bU",
          },
        };
        const mockRes = {
          status: jest.fn().mockReturnThis(),
          json: jest.fn().mockReturnThis(),
        };
        const mockNext = jest.fn();
  
        const authentication = new AuthenticationController({
          userModel: mockUser,
          roleModel: mockRole,
          bcrypt,
          jwt,
        });
  
        authentication.authorize("CUSTOMER")(mockReq, mockRes, mockNext);
  
        expect(mockNext).toHaveBeenCalled();
      });
      test("it should return 401 if the token role is not the same wit the required role", () => {
        const mockRole = new Role({
          id: 1,
          name: "CUSTOMER",
        });
        const mockUser = new User({
          id: 5,
          name: "example1",
          email: "example@email.com",
          image: null,
          encryptedPassword: "password",
          roleId: 2,
        });
  
        const mockReq = {
          headers: {
            authorization:
              "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6NywibmFtZSI6ImZyaXphIiwiZW1haWwiOiJleGFtcGxlQGVtYWlsLmNvbSIsImltYWdlIjpudWxsLCJyb2xlIjp7ImlkIjoxLCJuYW1lIjoiQ1VTVE9NRVIifSwiaWF0IjoxNjU0OTM5MjY5fQ.J3zzqC14IbX-Ih0oVt8ThL00EVS5nTfgCP7CzFK0-bU",
          },
        };
        const mockRes = {
          status: jest.fn().mockReturnThis(),
          json: jest.fn().mockReturnThis(),
        };
        const mockNext = jest.fn();
  
        const authentication = new AuthenticationController({
          userModel: mockUser,
          roleModel: mockRole,
          bcrypt,
          jwt,
        });
  
        const err = new InsufficientAccessError(mockRole.name);
  
        authentication.authorize("ADMIN")(mockReq, mockRes, mockNext);
  
        expect(mockRes.status).toHaveBeenCalledWith(401);
        expect(mockRes.json).toHaveBeenCalledWith({
          error: {
            name: err.name,
            message: err.message,
            details: err.details || null,
          },
        });
      });
    });
    describe("#handleLogin", () => {
      test("should call res.status(201) and accessToken if the credential is correct", async () => {
        const mockReq = {
          body: {
            email: "example@examplemail.com",
            password: "examplepassword",
          },
        };
        const mockRes = {
          status: jest.fn().mockReturnThis(),
          json: jest.fn().mockReturnThis(),
        };
        const next = jest.fn();
  
        const mockRole = new Role({
          id: 2,
          name: "ADMIN",
        });
        const mockUser = new User({
          id: 1,
          name: "example",
          email: "example@examplemail.com",
          image: "image.jpg",
          encryptedPassword:
            "$2a$12$ctZzZfG1WXb9U5G0hs/v0e94ybbn3TmWAQ13O8/AyugMIQtk9l2.S",
          roleId: 2,
        });
  
        const mockUserModel = {
          findOne: jest.fn().mockReturnValue({
            ...mockUser.dataValues,
            Role: mockRole,
          }),
        };
  
        const authentication = new AuthenticationController({
          userModel: mockUserModel,
          roleModel: mockRole,
          bcrypt,
          jwt,
        });
  
        await authentication.handleLogin(mockReq, mockRes, next);
        expect(mockUserModel.findOne).toHaveBeenCalledWith({
          where: { email: mockReq.body.email.toLowerCase() },
          include: [{ model: mockRole, attributes: ["id", "name"] }],
        });
        expect(mockRes.status).toHaveBeenCalledWith(201);
        expect(mockRes.json).toHaveBeenCalledWith({
          accessToken: expect.any(String),
        });
      });
      test("should call res.status(401) if the password credential is incorrect", async () => {
        const mockReq = {
          body: {
            email: "example@examplemail.com",
            password: "examplepassword",
          },
        };
        const mockRes = {
          status: jest.fn().mockReturnThis(),
          json: jest.fn().mockReturnThis(),
        };
        const next = jest.fn();
  
        const mockRole = new Role({
          id: 2,
          name: "ADMIN",
        });
  
        const mockUserModel = {
          findOne: jest.fn().mockReturnValue({
            id: 1,
            name: "example",
            email: "example@examplemail.com",
            image: "image.jpg",
            encryptedPassword: "insertwrongpasswordintentionally",
            roleId: 2,
          }),
        };
  
        const authentication = new AuthenticationController({
          userModel: mockUserModel,
          roleModel: mockRole,
          bcrypt,
          jwt,
        });
  
        const err = new WrongPasswordError();
        await authentication.handleLogin(mockReq, mockRes, next);
        expect(mockUserModel.findOne).toHaveBeenCalledWith({
          where: { email: mockReq.body.email.toLowerCase() },
          include: [{ model: mockRole, attributes: ["id", "name"] }],
        });
        expect(mockRes.status).toHaveBeenCalledWith(401);
        expect(mockRes.json).toHaveBeenCalledWith(err);
      });

      test("should call res.status(404) if the email credential is incorrect", async () => {
        const mockReq = {
          body: {
            email: "example@examplemail.com",
            password: "examplepassword",
          },
        };
        const mockRes = {
          status: jest.fn().mockReturnThis(),
          json: jest.fn().mockReturnThis(),
        };
        const next = jest.fn();
  
        const mockRole = new Role({
          id: 2,
          name: "ADMIN",
        });
  
        const mockUserModel = {
          findOne: jest.fn().mockReturnValue(null),
        };
  
        const authentication = new AuthenticationController({
          userModel: mockUserModel,
          roleModel: mockRole,
          bcrypt,
          jwt,
        });
  
        const err = new EmailNotRegisteredError(mockReq.body.email);
        await authentication.handleLogin(mockReq, mockRes, next);
        expect(mockUserModel.findOne).toHaveBeenCalledWith({
          where: { email: mockReq.body.email },
          include: [{ model: mockRole, attributes: ["id", "name"] }],
        });
        expect(mockRes.status).toHaveBeenCalledWith(404); 
        expect(mockRes.json).toHaveBeenCalledWith(err);
      });
    });
  
    describe("#handleGetUser", () => {
      test("it should be return 404 status error if the user is not found", async () => {
        const mockReq = {
          user: {
            id: 1,
          },
        };
        const mockRes = {
          status: jest.fn().mockReturnThis(),
          json: jest.fn().mockReturnThis(),
        };
        const mockRole = new Role({
          id: 2,
          name: "ADMIN",
        });
  
        const mockUser = new User({
          id: 1,
          name: "example",
          email: "example@examplemail.com",
          image: "image.jpg",
          encryptedPassword:
            "$2a$12$ctZzZfG1WXb9U5G0hs/v0e94ybbn3TmWAQ13O8/AyugMIQtk9l2.S",
          roleId: 2,
        });
  
        const mockUserModel = {
          ...mockUser.dataValues,
          findByPk: jest.fn().mockReturnValue(null),
        };
  
        const authentication = new AuthenticationController({
          userModel: mockUserModel,
          roleModel: mockRole,
          bcrypt,
          jwt,
        });
  
        const err = new RecordNotFoundError(mockUser.name);
        await authentication.handleGetUser(mockReq, mockRes);
  
        expect(mockUserModel.findByPk).toHaveBeenCalledWith(mockReq.user.id);
        expect(mockRes.status).toHaveBeenCalledWith(404);
        expect(mockRes.json).toHaveBeenCalledWith(err);
      });
  
      test("it should be return 404 status error if the role is not found", async () => {
        const mockReq = {
          user: {
            id: 1,
          },
        };
        const mockRes = {
          status: jest.fn().mockReturnThis(),
          json: jest.fn().mockReturnThis(),
        };
        const mockRole = new Role({
          id: 2,
          name: "ADMIN",
        });
  
        const mockUser = new User({
          id: 1,
          name: "example",
          email: "example@examplemail.com",
          image: "image.jpg",
          encryptedPassword:
            "$2a$12$ctZzZfG1WXb9U5G0hs/v0e94ybbn3TmWAQ13O8/AyugMIQtk9l2.S",
          roleId: 2,
        });
  
        const mockUserModel = {
          ...mockUser.dataValues,
          findByPk: jest.fn().mockReturnValue(mockUser),
        };
  
        const mockRoleModel = {
          ...mockRole.dataValues,
          findByPk: jest.fn().mockReturnValue(null),
        };
  
        const authentication = new AuthenticationController({
          userModel: mockUserModel,
          roleModel: mockRoleModel,
          bcrypt,
          jwt,
        });
  
        const err = new RecordNotFoundError(mockRole.name);
  
        await authentication.handleGetUser(mockReq, mockRes);
  
        expect(mockUserModel.findByPk).toHaveBeenCalledWith(mockReq.user.id);
        expect(mockRoleModel.findByPk).toHaveBeenCalledWith(mockUser.roleId);
        expect(mockRes.status).toHaveBeenCalledWith(404);
        expect(mockRes.json).toHaveBeenCalledWith(err);
      });
  
      test("it should be return 200 status code", async () => {
        const mockReq = {
          user: {
            id: 1,
          },
        };
        const mockRes = {
          status: jest.fn().mockReturnThis(),
          json: jest.fn().mockReturnThis(),
        };
        const mockRole = new Role({
          id: 2,
          name: "ADMIN",
        });
  
        const mockUser = new User({
          id: 1,
          name: "example",
          email: "example@examplemail.com",
          image: "image.jpg",
          encryptedPassword:
            "$2a$12$ctZzZfG1WXb9U5G0hs/v0e94ybbn3TmWAQ13O8/AyugMIQtk9l2.S",
          roleId: 2,
        });
  
        const mockUserModel = {
          ...mockUser.dataValues,
          findByPk: jest.fn().mockReturnValue(mockUser),
        };
  
        const mockRoleModel = {
          ...mockRole.dataValues,
          findByPk: jest.fn().mockReturnValue(mockUser.roleId),
        };
  
        const authentication = new AuthenticationController({
          userModel: mockUserModel,
          roleModel: mockRoleModel,
          bcrypt,
          jwt,
        });
  
        await authentication.handleGetUser(mockReq, mockRes);
        expect(mockUserModel.findByPk).toHaveBeenCalledWith(mockReq.user.id);
        expect(mockRoleModel.findByPk).toHaveBeenCalledWith(mockUser.roleId);
        expect(mockRes.status).toHaveBeenCalledWith(200);
        expect(mockRes.json).toHaveBeenCalledWith(mockUser);
      });
    });
  
    describe("#handleRegister", () => {
      test("it should be return 422 code if email is already taken", async () => {
        const mockReq = {
          body: {
            name: "example",
            email: String("example@examplemail.com").toLowerCase(),
            password: "examplepassword",
          },
        };
        const mockRes = {
          status: jest.fn().mockReturnThis(),
          json: jest.fn().mockReturnThis(),
        };
        const next = jest.fn();
  
        const mockUser = new User({
          id: 1,
          name: "example",
          email: "example@examplemail.com",
          image: "image.jpg",
          encryptedPassword:
            "$2a$12$ctZzZfG1WXb9U5G0hs/v0e94ybbn3TmWAQ13O8/AyugMIQtk9l2.S",
          roleId: 2,
        });
        const mockRole = new Role({
          id: 2,
          name: "ADMIN",
        });
  
        const mockUserModel = {
          findOne: jest.fn().mockReturnValue(mockUser),
        };
  
        const err = new EmailAlreadyTakenError(mockReq.body.email);
        const authentication = new AuthenticationController({
          userModel: mockUserModel,
          roleModel: mockRole,
          bcrypt,
          jwt,
        });
  
        await authentication.handleRegister(mockReq, mockRes, next);
        expect(mockUserModel.findOne).toHaveBeenCalledWith({
          where: { email: mockReq.body.email },
        });
        expect.anything(mockRes.status, 422);
        expect.anything(mockRes.json, err);
      });
  
      test("it should run next if the promise is rejected", async () => {
        const mockReq = {
          body: {
            name: "example",
            email: String("example@examplemail.com").toLowerCase(),
            password: "examplepassword",
          },
        };
        const mockRes = {
          status: jest.fn().mockReturnThis(),
          json: jest.fn().mockReturnThis(),
        };
        const next = jest.fn();
  
        const mockUserModel = {
          findOne: jest.fn(() => Promise.reject(err)),
        };
        const mockRole = new Role({
          id: 2,
          name: "ADMIN",
        });
  
        const authentication = new AuthenticationController({
          userModel: mockUserModel,
          roleModel: mockRole,
          bcrypt,
          jwt,
        });
  
        await authentication.handleRegister(mockReq, mockRes, next);
  
        expect(next).toHaveBeenCalled();
      });
    });
  
    test("it should return 201 status code and some accessToken", async () => {
      const mockReq = {
        body: {
          name: "example",
          email: String("example@examplemail.com").toLowerCase(),
          password: "examplepassword",
        },
      };
      const mockRes = {
        status: jest.fn().mockReturnThis(),
        json: jest.fn().mockReturnThis(),
      };
      const next = jest.fn();
  
      const mockUser = new User({
        id: 1,
        name: "example",
        email: "example@examplemail.com",
        image: "image.jpg",
        encryptedPassword:
          "$2a$12$ctZzZfG1WXb9U5G0hs/v0e94ybbn3TmWAQ13O8/AyugMIQtk9l2.S",
        roleId: 2,
      });
      const mockRole = new Role({
        id: 2,
        name: "CUSTOMER",
      });
  
      const mockRoleModel = {
        findOne: jest.fn().mockReturnValue(mockRole.name),
      };
      const mockUserModel = {
        findOne: jest.fn().mockReturnValue(null),
        create: jest.fn().mockReturnValue(mockUser),
      };
  
      const authentication = new AuthenticationController({
        userModel: mockUserModel,
        roleModel: mockRoleModel,
        bcrypt,
        jwt,
      });
      await authentication.handleRegister(mockReq, mockRes, next);
  
      expect(mockUserModel.findOne).toHaveBeenCalledWith({
        where: { email: mockReq.body.email },
      });
      expect(mockUserModel.create).toHaveBeenCalled();
      expect(mockRoleModel.findOne).toHaveBeenCalledWith({
        where: { name: mockRole.name },
      });
      expect(mockRes.status).toHaveBeenCalledWith(201);
      expect(mockRes.json).toHaveBeenCalledWith({
        accessToken: expect.any(String),
      });
    });
  });
  